package com.xyz.framework.utils;


import com.xyz.framework.dto.RtnMessage;
import org.apache.commons.lang3.StringUtils;

/**
 * 构造RtnMessage
 */
public class RtnMessageUtils {
    /**
     * 构造成功的结果
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> RtnMessage<T> buildSuccess(T data) {
        RtnMessage<T> t = new RtnMessage<T>(data);
        return t;
    }

    /**
     * 构造失败的结果
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> RtnMessage<T> buildFailed(T data) {
        RtnMessage<T> t = new RtnMessage<T>(RtnMessage.FAILED, "失败", data);
        return t;
    }

    /**
     * 构造失败结果
     *
     * @param code
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> RtnMessage<T> buildError(String code, String msg) {
        RtnMessage<T> t = new RtnMessage<T>(code, msg, null);
        return t;
    }


    public static <T> RtnMessage<T> buildFailedWithMsg(String msg) {
        RtnMessage<T> t = new RtnMessage<T>(RtnMessage.FAILED, "失败", null);
        t.setMessage(msg);
        return t;
    }

    public static <T> RtnMessage<T> paramValidateFailed(String defaultMessage) {
        RtnMessage<T> result = new RtnMessage<T>(RtnMessage.FAILED, "参数校验失败", null);
        if (StringUtils.isNotBlank(defaultMessage)) {
            result.setMessage(defaultMessage);
        }
        return result;
    }

    /**
     * @param
     * @return
     * @author yuerfeng 14090408
     * @Description 复制message
     * @since 2019-07-18 20:03
     **/
    public static <T> RtnMessage<T> buildFailedFromRtn(RtnMessage rtnMessage) {
        if (rtnMessage == null) {
            return buildFailed(null);
        }
        RtnMessage<T> t = new RtnMessage<T>(RtnMessage.FAILED, "失败", null);
        t.setMessage(rtnMessage.getMessage());
        t.setCode(rtnMessage.getCode());
        return t;
    }

}
