package com.xyz.framework.utils;

import com.google.common.collect.Maps;
import com.xyz.framework.annotations.FieldNameConvert;
import com.xyz.framework.dto.BaseEo;
import com.xyz.framework.dto.BaseVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ConvertUtils {
    private static Logger logger = LoggerFactory.getLogger(ConvertUtils.class);
    /**
     * ReqXX转POJO通用方法
     *
     * @param reqObj
     * @param pojoCls
     * @param <T>
     * @return
     */
    public static <T> T convert(Object reqObj, Class<T> pojoCls) {
        try {
            if (reqObj == null) {
                return pojoCls.newInstance();
            }
            T target = pojoCls.newInstance();
            //浅拷贝基本信息
            BeanUtils.copyProperties(reqObj, target);
            return target;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T extends BaseEo,U extends BaseVo> T vo2Eo(U reqObj, Class<T> pojoCls){
        try {
            if (reqObj == null) {
                return pojoCls.newInstance();
            }
            T target = pojoCls.newInstance();
            //浅拷贝基本信息
            BeanUtils.copyProperties(reqObj, target);
            target.getExtMap().putAll(reqObj.getExtMap());
            return target;
        } catch (Exception e) {
            return null;
        }
    }
    public static <T extends BaseEo,U extends BaseVo> T vo2Eo(U reqObj, Class<T> pojoCls, Boolean voAsMap){
        try {
            if (reqObj == null) {
                return pojoCls.newInstance();
            }
            T target = pojoCls.newInstance();
            //浅拷贝基本信息
            BeanUtils.copyProperties(reqObj, target);
            //将req的字段作为extMap的字段传递
            if(voAsMap){
                Map<String, Object> stringObjectMap = beanToMap(reqObj);
                reqObj.getExtMap().putAll(stringObjectMap);
            }
            target.getExtMap().putAll(reqObj.getExtMap());
            return target;
        } catch (Exception e) {
            return null;
        }
    }
    public static <T> T convertDifferentField(Object reqObj, Class<T> pojoCls){
        try {
            if (reqObj == null) {
                return pojoCls.newInstance();
            }
            T target = pojoCls.newInstance();
            //浅拷贝基本信息
            BeanUtils.copyProperties(reqObj, target);
            converField(reqObj, target, pojoCls.getDeclaredFields());
            return target;
        } catch (Exception e) {
            logger.info("转换异常！" + e);
            return null;
        }

    }

    private static <T> void converField(Object reqObj, T target, Field[] declaredFields2) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        Field[] declaredFields = declaredFields2;
        if (declaredFields != null && declaredFields.length > 0) {
            for (Field field : declaredFields) {
                FieldNameConvert annotation = field.getAnnotation(FieldNameConvert.class);
                if (annotation != null) {
                    String value = annotation.value();
                    Field declaredField = reqObj.getClass().getDeclaredField(value);
                    declaredField.setAccessible(true);
                    Object o = declaredField.get(reqObj);
                    String s = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    Method method = target.getClass().getMethod("set" + s, new Class[]{field.getType()});
                    method.invoke(target, o);
                }
            }
        }
    }


    public static <T,U> List<T> convertAllDifferentField(List<U> objects, Class<T> rtnCls) {
        try {
            if (objects == null || objects.isEmpty()) {
                return new LinkedList<>();
            }
            List<T> result = new LinkedList<>();
            for(U obj : objects){
                T target = rtnCls.newInstance();
                //浅拷贝基本信息
                BeanUtils.copyProperties(obj, target);
                converField(obj, target, rtnCls.getDeclaredFields());
                result.add(target);
            }
            return result;
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }

    public static <T,U> List<T> convertAll(List<U> objects, Class<T> rtnCls) {
        try {
            if (objects == null || objects.isEmpty()) {
                return new LinkedList<>();
            }
            List<T> result = new LinkedList<>();
            for(U obj : objects){
                T target = rtnCls.newInstance();
                //浅拷贝基本信息
                BeanUtils.copyProperties(obj, target);
                result.add(target);
            }
            return result;
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }

    public static String convertList2Str(List<String> list){
        if (CollectionUtils.isEmpty(list)){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            sb.append(str);
            sb.append(",");
        }
        return sb.toString();
    }

    /**
     * @author:Jason
     * :每个元素加单引号,逗号分隔
     * @Param [list]
     * @return java.lang.String
     **/
    public static String convertList2StrAp(List<String> list){

        if (CollectionUtils.isEmpty(list)){
            return "''";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0;i < list.size();i++) {
            sb.append("'" + list.get(i) + "'");
            if (i != list.size() - 1){
                sb.append(",");
            }
        }

        return sb.toString();
    }

    public static List<String> convertStr2List(String str){
        if (StringUtils.isEmpty(str)){
            return null;
        }
        String[] split = str.split(",");
        List<String> strings = new LinkedList<>();
        for (int i = 0; i < split.length; i++) {
            strings.add(split[i]);
        }
        return strings;
    }

    /**
     * 将对象装换为map
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMap(T bean) {
        Map<String, Object> map = Maps.newHashMap();
        try {
            if (bean != null) {
                BeanMap beanMap = BeanMap.create(bean);
                beanMap.keySet().forEach((key)->{
                    String strKey = key + "";
                    Object val = beanMap.get(key);
                    if(val == null){
                        return;
                    }
                    if(StringUtils.equals("pageNo",strKey)
                    || StringUtils.equals("pageSize",strKey)){
                        return;
                    }
                    if(org.apache.commons.lang3.StringUtils.equals("extMap",strKey)
                            && val instanceof Map){
                        map.putAll((Map)val);
                    }else{
                        map.put(strKey, beanMap.get(key));
                    }
                });

            }
        } catch (Exception e){
            logger.info("对象转map异常", e);
        }
        return map;
    }

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        try {
            BeanMap beanMap = BeanMap.create(bean);
            beanMap.putAll(map);
        } catch (Exception e){
            logger.info("map装换为javabean对象异常", e);
        }
        return bean;
    }
}
