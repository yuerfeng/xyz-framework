package com.xyz.framework.page;

/**
 * @author yuerfeng 14090408
 * @since 2020-01-16
 **/
public interface IPageEntity {

    Integer getPageNo();

    void setPageNo(Integer pageNo);

    Integer getPageSize();

    void setPageSize(Integer pageSize);
}
