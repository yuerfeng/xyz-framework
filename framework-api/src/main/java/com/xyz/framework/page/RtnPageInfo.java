package com.xyz.framework.page;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuerfeng
 * @since 2019/7/8
 **/
@ApiModel("返回数据")
public class RtnPageInfo<T> implements Serializable {
    @ApiModelProperty("总条数")
    private Long total;
    @ApiModelProperty("分页数据")
    private List<T> list;

    public RtnPageInfo(Long total, List<T> list) {
        this.total = total;
        this.list = list;
    }

    public RtnPageInfo() {
    }

    /**
     * 通过mybatisPlus的page构造RtnPageInfo
     *
     * @param page
     */
    public RtnPageInfo(Page<T> page) {
        this.total = page.getTotal();
        this.list = page.getRecords();
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
