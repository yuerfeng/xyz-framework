package com.xyz.framework.page;

import com.xyz.framework.dto.BaseVo;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author yuerfeng 14090408
 * @since 2020-01-16
 **/
public class PageParam extends BaseVo implements IPageEntity {
    @ApiModelProperty("分页参数")
    protected Integer pageNo;
    @ApiModelProperty("分页参数")
    protected Integer pageSize;

    public PageParam(Integer pageNo, Integer pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    public PageParam() {
    }

    @Override
    public Integer getPageNo() {
        return pageNo;
    }

    @Override
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    @Override
    public Integer getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
