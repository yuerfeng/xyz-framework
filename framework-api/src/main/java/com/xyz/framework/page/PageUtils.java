package com.xyz.framework.page;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author yuerfeng 14090408
 * @since 2020-01-17
 **/
public class PageUtils {
    /**
     * @param
     * @return
     * @author yuerfeng 14090408
     * 参数转换
     * @since 2020-01-17 14:38
     **/
    public static <T> Page<T> buildPage(IPageEntity page) {
        if (page.getPageNo() == null) {
            page.setPageNo(0);
        }
        if (page.getPageSize() == null || page.getPageSize() == 0) {
            page.setPageSize(10);
        }

        Page<T> objectPage = new Page<>();
        objectPage.setCurrent(page.getPageNo());
        objectPage.setSize(page.getPageSize());
        return objectPage;
    }

    /**
     * 分页返回结果转换
     *
     * @param page
     * @param <T>
     * @return
     */
    public static <T> RtnPageInfo<T> convertPageResult(IPage<T> page) {
        RtnPageInfo<T> result = new RtnPageInfo<>();
        result.setList(page.getRecords());
        result.setTotal(page.getTotal());
        return result;
    }
}
