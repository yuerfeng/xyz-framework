package com.xyz.framework.dto;


import org.apache.commons.lang3.StringUtils;

/**
 * dubbo接口返回通用类型
 *
 * @param <T> t 为自定义返回内容根据业务需要自己定义
 * @author yuerfeng
 * @create 2018-6-22
 */
public class RtnMessage<T> {
    public final static String SUCCESS = "0";
    public final static String FAILED = "10000";

    /**
     * 业务码  详见ErrorType
     */
    private String code;

    /**
     * 信息
     */
    private String message;

    /**
     * 数据
     */
    private T data;

    public RtnMessage() {
        this.code = SUCCESS;
        this.message = "成功";
    }

    public RtnMessage(T data) {
        this.code = SUCCESS;
        this.message = "成功";
        this.data = data;
    }

    /**
     * 推荐使用RtnMessageUtils来构造，而不要自己去new RtnMessage
     *
     * @param code
     * @param msg
     * @param data
     */
    public RtnMessage(String code, String msg, T data) {
        this.code = code;
        this.message = msg;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return StringUtils.equals(this.code, SUCCESS);
    }
}
