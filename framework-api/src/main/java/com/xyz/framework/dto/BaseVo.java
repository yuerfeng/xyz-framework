package com.xyz.framework.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yuerfeng 14090408
 *
 * @since 2020-05-10
 **/
public class BaseVo implements Serializable {
    private Map<String,Object> extMap = new HashMap<>();

    public Map<String, Object> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<String, Object> extMap) {
        this.extMap = extMap;
    }

    public String getExtMapString(String name){
        Object o = getExtMap().get(name);
        if(o == null){
            return "";
        }
        return o.toString();
    }

    public <T> T getExtMapData(String name){
        Object o = getExtMap().get(name);
        if(o == null){
            return null;
        }
        return (T)o;
    }
}
