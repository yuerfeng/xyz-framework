package com.xyz.framework.annotations;

import java.lang.annotation.*;

/**
 * @author: Jason
 * @since: 2019/8/15
 * : 此注解用于转换类时属性名不一样
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface FieldNameConvert {

    String value();

}
